from django.shortcuts import render, redirect
from receipts.models import Receipt, ExpenseCategory, Account
from django.contrib.auth.decorators import login_required
from receipts.forms import (
    CreateReceiptForm,
    CreateCategoryForm,
    CreateAccountForm,
)


# View function for all the receipts to be shown at receipts/list.html
@login_required  # Protects the list view
def all_receipts(request):
    receipts = Receipt.objects.filter(purchaser=request.user)
    context = {
        "all_receipts": receipts,
    }
    return render(request, "receipts/list.html", context)


# View function for 'Receipt' model to create a new Receipt. Person must be logged in to see view.
@login_required
def create_receipt(request):
    if request.method == "POST":
        form = CreateReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.purchaser = request.user
            receipt.save()
            return redirect("home")
    else:
        form = CreateReceiptForm()

    context = {
        "form": form,
    }

    return render(request, "receipts/create.html", context)


# View function for all the expense categories to be shown at categories/list.html
def category_list(request):
    categories = ExpenseCategory.objects.filter(owner=request.user)
    context = {
        "category_list": categories,
    }
    return render(request, "categories/list.html", context)


# View function for all the accounts to be shown at accounts/list.html
def account_list(request):
    accounts = Account.objects.filter(owner=request.user)
    context = {
        "account_list": accounts,
    }
    return render(request, "accounts/list.html", context)


# View function for 'ExpenseCategory' model to create a new Expense Category. Person must be logged in to see view.
@login_required
def create_category(request):
    if request.method == "POST":
        form = CreateCategoryForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.owner = request.user
            receipt.save()
            return redirect("category_list")
    else:
        form = CreateCategoryForm()

    context = {
        "form": form,
    }

    return render(request, "categories/create.html", context)


# View function for 'Account' model to create a new Account. Person must be logged in to see view.
@login_required
def create_account(request):
    if request.method == "POST":
        form = CreateAccountForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.owner = request.user
            receipt.save()
            return redirect("account_list")
    else:
        form = CreateAccountForm()

    context = {
        "form": form,
    }

    return render(request, "accounts/create.html", context)
