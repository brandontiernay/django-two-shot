from django.shortcuts import render, redirect
from django.contrib.auth import login, authenticate, logout
from accounts.forms import UserLoginForm, UserSignupForm
from django.contrib.auth.models import User


# View function that will use 'class LoginForm' on accounts/forms.py
def user_login(request):
    if request.method == "POST":
        form = UserLoginForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]

            user = authenticate(
                request,
                username=username,
                password=password,
            )
            if user is not None:
                login(request, user)
                return redirect("home")
    else:
        form = UserLoginForm()

    context = {"form": form}

    return render(request, "login/detail.html", context)


# View function that will log users out
def user_logout(request):
    logout(request)
    return redirect("login")


# View function that will handle 'class UserSignupForm' found on accounts/forms.py
def user_signup(request):
    if request.method == "POST":
        form = UserSignupForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            password_confirmation = form.cleaned_data["password_confirmation"]

            if password == password_confirmation:
                # Create a new user with those values and save it to a variable
                user = User.objects.create_user(
                    username,
                    password=password,
                )
                # Login the user with the user you just created
                login(request, user)

                return redirect("home")
            else:
                form.add_error("password", "the passwords do not match")
    else:
        form = UserSignupForm()
        context = {
            "form": form,
        }
        return render(request, "accounts/signup.html", context)
